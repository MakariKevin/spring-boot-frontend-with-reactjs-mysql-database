A simple application using React as frontend and spring boot as backend with MySQL database. 

This specific one is React-frontend (client) – Consumes REST API. Check out [Its Backend Implementation](https://gitlab.com/MakariKevin/spring-boot-backend-with-reactjs-mysql-database)

## Tools And Technologies Used
- React JS
- Material UI
- MySQL Database
- VS Code

## Output

![Screenshot 2022-11-06 at 03.59.16.png](./Screenshot 2022-11-06 at 03.59.16.png)

![Screenshot 2022-11-06 at 03.59.33.png](./Screenshot 2022-11-06 at 03.59.33.png)

![Screenshot 2022-11-06 at 03.32.57.png](./Screenshot 2022-11-06 at 03.32.57.png)
